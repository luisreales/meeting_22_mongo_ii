var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CommentSchema = new Schema({
    user: String,
    comment: {type: String, trim: true},
    created: {type: Date, default: Date.now(), select: false}
});

var PostSchema = new Schema({
    text: {type: String, trim: true},
    postedBy: String,
    comments: [CommentSchema]
});

module.exports = mongoose.model('Post', PostSchema);